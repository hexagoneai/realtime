import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

class RealTimeConsumer(WebsocketConsumer):
    recording_group_name = None
    recording_id = None

    def connect(self):
        self.recording_group_name = None
        self.recording_id = None

        self.recording_id = self.scope['url_route']['kwargs']['recording_id']
        self.recording_group_name = 'recording_%s' % self.recording_id

        # Join recording group
        async_to_sync(self.channel_layer.group_add)(
            self.recording_group_name,
            self.channel_name
        )

        print(f'Channel name = {self.channel_name}')

        self.accept()

    def disconnect(self, _):
        if self.recording_group_name and self.channel_name:
            async_to_sync(self.channel_layer.group_discard)(
                self.recording_group_name,
                self.channel_name
            )

    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        data = text_data_json.get('data', None)
        message_type = text_data_json.get('type', 'upload_chunk')

        async_to_sync(self.channel_layer.group_send)(
            self.recording_group_name,
            {
                'type': message_type,
                'data': data,
            }
        )

    def upload_chunk(self, event):
        pass

    def save_recording(self, event):
        pass