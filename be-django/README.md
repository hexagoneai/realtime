# Introduction

The goal of this project is to provide minimalistic django project template to test our candidate. 

# Getting Started
    
You need to have [python 3](https://www.python.org/download/releases/3.0/) and [pip](https://pip.pypa.io/en/stable/) installed before continuing.

Activate the virtualenv for your project.
    
Install project dependencies:

    $ pip install -r requirements.txt
    
    
Then simply apply the migrations:

    $ python manage.py migrate
    

You can now run the development server:

    $ python manage.py runserver

# Dependencies

This exercice use [Django channels](https://channels.readthedocs.io/en/stable/)
to handle WSS connections
