import { Injectable } from '@angular/core';
import { webSocket }  from 'rxjs/webSocket';
import {environment}  from "../environments/environment";

@Injectable({
              providedIn: 'root'
            })
export class RealtimeService {
  ws_url: string;
  connections = {};

  constructor() {
    this.ws_url = environment.ws_base_url +  "/ws/realtime/";
  }

  connect(id: string) {
    this.connections[id] = webSocket(this.ws_url + id + '/');
  }

  getObservable(id: string) {
    return this.connections[id].asObservable();
  }

  send(id, payload) {
    this.connections[id].next(payload);
  }
}
